import {Socket} from "net"

export function send_json(json, ip, port){
    let socket = new Socket();
    console.log("Sending to", ip, port, json.request);
    socket.connect(port, ip);
    socket.on('connect', () => {
        socket.write(JSON.stringify(json));
        socket.destroySoon();
    });
}