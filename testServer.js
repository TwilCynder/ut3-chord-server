//import { ChordServer } from "./chordNode.js";
import {Server} from "net";

let server = new Server();

server.listen(5454);

server.on('listening', ()=> {
    let address = server.address();
    let port = address.port;
    let ip = address.address;
    console.log(`Server listening on ${ip} ; ${port} !`);
});

server.on('connection', (socket) => {
    socket.on('data', (data) => {
        let json = JSON.parse(data);
        console.log("Received request : ");
        console.log(json);
        let address = server.address();
        let port = address.port;
        let ip = address.address;
        console.log(ip, port);
    });
});