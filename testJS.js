class C{
    #a = 12;
    #node = this;

    test(){
        console.log(this.#a, this.a, this.#node);
    }
};

let obj = new C();
obj.test();

function t([a, b, c]){
    console.log(a, b, c);
}

t([1, 2, 3]);