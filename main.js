import { ChordServer } from "./chordNode.js";
import cl from '@twilcynder/commandLine';

if (process.argv.length < 4){
    console.log(`Usage : node ${process.argv[1]} my_ip my_port [entry_ip entry_port]`);
    process.exit(2);
}

let serv = new ChordServer(process.argv[2], process.argv[3]);

serv.on('init', (id) => {
    console.log("Chord node initialized with ID", id);
    serv.print_info();
});

switch (process.argv.length){
    case 4:
        console.log("Starting as the initial node");
        serv.start_inital_server();
        break;
    case 5:
        console.log(`Usage : node ${process.argv[1]} my_ip my_port [entry_ip entry_port]`);
        process.exit(2);
    case 6:
        console.log("Joining network");
        console.log(process.argv[4], process.argv[5]);
        serv.join_network(process.argv[4], process.argv[5]);
}

cl.commands = {
    print(){
        serv.print_info();
    },

    print_property(prop){
        console.log(serv[prop]);
    },

    print_link(){
        serv.print_link();
    },

    print_data(){
        serv.print_value();
    }
};

cl.start();