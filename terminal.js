import { send_json } from "./utility.js";
import cl from '@twilcynder/commandline';
import {Server} from "net";

if (process.argv.length < 4){
    console.error(`Usage : node ${process.argv[1]} local_ip local_port [(set key value | get key) dist_ip dist_port]`)
    process.exit(1);
}

let ip = process.argv[2];
let port = process.argv[3];

let server;

function init(port){
    server = new Server();
    server.listen(port);

    server.on('connection', (socket) => {
        socket.on('data', (data) => {
            let json = JSON.parse(data);
            if (json.request = 'RESPONSE'){
                if (json.present){
                    console.log(`La donnée d'ID ${json.id} a pour valeur ${json.value}`);
                } else {
                    console.log(`Aucune donnée ne possède l'ID ${json.id}`)
                }
            }
        })
    })
}

function set(key, value, ip, port){
    send_json({
        request: 'SET',
        id: key,
        value: value
    }, ip, port);
}

function get(key, dist_ip, dist_port){
    send_json({
        request: 'QUERY',
        ip: ip,
        port: port
    }, dist_ip, dist_port);
}

if (process.argv.length < 5){
    //interactive mode
} else {
    switch (process.argv[4]){
        case "set":
            if (process.argv.length < 9){
                console.error(`Usage : node ${process.argv[1]} local_ip local_port set key value dist_ip dist_port]`)
                process.exit(1);
            }
            set(process.argv[5], process.argv[6], process.argv[7], process.argv[8]);
            break;
        case "get":
            if (process.argv.length < 8){
                console.error(`Usage : node ${process.argv[1]} local_ip local_port get key dist_ip dist_port]`)
                process.exit(1);
            }
            get(process.argv[5], process.argv[6], process.argv[7]);
            init(port);
            break;
        default:
            console.error("Unknown command", process.argv[5]);
    }
}