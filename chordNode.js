//le noeud responsable d'une donnée est le suivant, un noeud est responsable des data qui le précèdent
//TODO utiiser getArg
//TODO obtenir la bonne IP

//Pour la version avec table de voisinnage étendue :
//- pour le message update: une fois qu'il est passé dans l'aute hémisphère, on jette

import { EventEmitter } from "events";
import { send_json } from "./utility.js";
import {Server} from "net";

var maxID = 16;

class RemoteNode {
    constructor(id, ip, port){
        this.id = id;
        this.ip = ip;
        this.port = port;
    }
}

function checkString(arg){
    if (typeof arg != "string") throw {
        message: "Invalid argument type"
    };
}

function getArg(obj, name, type){
    let arg = obj[name];
    if (arg == undefined || arg == null) throw "Obj does not have a property " + name;
    if (type && typeof arg != type) throw `Wrong type for property ${name} : expected ${type}, got ${typeof arg}`;
    return arg;
}

function getArgs(obj, ...names){
    let res = [];
    for (let n of names){
        res.push(getArg(obj, n));
    }
    return res;
}

function getArgsNamed(obj, ...names){
    let res = {}
    for (k of names){
        res[k] = getArg(obj, k);
    }
    return res;
}

function send_to_node(node, json){
    send_json(json, node.ip, node.port);
}

function node_string(node){
    return `ID : ${node.id} | IP : ${node.ip} | Port : ${node.port}`;
}

class Data {
    #data = {};

    get(key){
        return this.#data[key];
    }

    put(key, value){ //same
        this.#data[key] = value;
    }

    split_data(id){
        let ext_data = {}
        let new_data = {}
        for (const [k, v] of Object.entries(this.#data)){
            if (k <= id && k > this.id) {
                ext_data[k] = v;
            } else {
                new_data[k] = v;
            }
        }
        return ext_data;
    }

    print(){
        for (let k in this.#data){
            console.log(k, this.#data[k]);
        }
    }

}

export class ChordServer extends EventEmitter {
    #data = new Data();

    #neighbors = null;

    #server = new Server();
    #initialized = false;

    #entry;

    constructor(ip, port){
        super();

        let self = this;

        this.port = port;
        this.ip = ip;
        this.id = null;

        this.#initHandlers();

        this.#server.on('listening', ()=>{

            let address = this.#server.address();
            self.port = address.port;
            self.ip = address.address;
            console.log(`Server listening on ${this.ip}:${this.port} !`);
        });

        this.#server.on('connection', (socket) => {
            socket.on('data', (data) => {
                let json = JSON.parse(data);
                console.log("Received request : ");
                console.log(json);
                console.log("remote : ", socket.localAddress, socket.localPort)
                this.#interpretRequest(json);
            });
        });
        
        this.#server.listen(port);
    }

    #amIResponsibleOf(id){
        let prev = this.#neighbors.previous;
        console.log(id, this.#neighbors.previous.id, (id <= this.id && id > prev.id) || (prev.id > this.id && id > prev.id));
        this.print_link();
        return (id <= this.id && id > prev.id) || (prev.id > this.id && (id > prev.id || id <= this.id));
    }

    #onJoin({id, ip_pred, port_pred, id_pred, ip_succ, port_succ, id_succ, data}){
        this.id = id;
        this.#neighbors = {
            previous: new RemoteNode(id_pred, ip_pred, port_pred),
            next : new RemoteNode(id_succ, ip_succ, port_succ)
        };
        data = data; //?

        this.print_link();

        send_to_node(this.#neighbors.previous, {
            request: 'UPDATE',
            ip: this.ip,
            port: this.port,
            id: this.id
        });

        this.emit('init', id);
    }

    /**
     * If this node is responsible for the given id, calls the callback with the id and other specified args.
     * If it's not, send the json to the next node.
     * @param {object} json the message to transmit (or not)
     * @param {function} callback function to call if this node is responsible for the given id. Called with the node as this, and id followed by args as arguments
     * @param {number} id id to check
     * @param  {...any} args additional arguments to pass to the callback
     */
    #generic_lookup(json, callback, id, args){
        if (this.#amIResponsibleOf(id)){
            callback.apply(this, [id].concat(args));
        } else {
            send_to_node(this.#neighbors.next, json);
        }
    }

    /**
     * Checks whether the given id can be attributed to a requesting node, and either
     * - forwards the check message if this node is not responsible for this id
     * - answers NAV or OK if we're the right node
     * @param {*} id id to check
     * @param {*} ip ip of the requesting node
     * @param {*} port port of the requesting node
     */

    #check(json){
        this.#generic_lookup(json, (id) => {
            let [ip, port] = getArgs(json, "ip", "port");
            if (id == this.id){
                send_json({
                    request: 'NAV'
                }, ip, port);
            } else {
                this.#accept_join(ip, port, id);
            }
        }, getArg(json, "id"));
    }

    /**
     * Attemps to add a new node to the network, following a request from this node
     * @param {string} ip adress of the requesting node
     * @param {number} port port of the requesting node
     */
    #add_node(ip, port){
        let rand_id;

        //generate a new id as long as we keep generating the same as ours
        while ((rand_id = Math.floor(Math.random() * maxID)) == this.id ){}

        if (this.#neighbors.next.id == this.id){ //means we are alone
            this.#accept_join(ip, port, rand_id);
            let node = new RemoteNode(rand_id, ip, port);
            this.#neighbors.next = node;
            this.#neighbors.previous = node;
        } else {
            this.print_link();
            if (this.#amIResponsibleOf(rand_id)){
                this.#accept_join(ip, port, rand_id);
            } else {
                send_to_node(this.#neighbors.next, {
                    request: 'CHECK',
                    ip: ip,
                    port: port,
                    id: rand_id
                });
            }
        }
    }

    /**
     * Validates the addition of a node. Informs this node and gives it its share of data.
     * @param {string} ip adress of the new node
     * @param {number} port port of the new node
     * @param {number} id id of the new node
     */
    #accept_join(ip, port, id){
        console.log("ACCEPT JOIN");

        let prev = this.#neighbors.previous;
        
        send_json({
            request: 'OK',
            ip_pred: prev.ip,
            port_pred: prev.port,
            id_pred: prev.id,
            ip_succ: this.ip,
            port_succ: this.port,
            id_succ: this.id,
            id: id,
            data: this.#data.split_data(id)
        }, ip, port);

        
        prev.id = id;
        prev.ip = ip;
        prev.port = port;
    }

    #onResponseRecevied(value, id){ //private, fa valloir fire un event qque chose comme ça
        console.log(`Value received for id ${id} : ${value} !`);
    }

    #onDataNotFound(id){ //private, fa valloir fire un event qque chose comme ça
        console.log(`No value found for id ${id}.`);
    }

    #handlers;
    #initHandlers(){
        let self = this;
        this.#handlers = {
            SET(json){
                if (!self.#initialized) return;
                self.#generic_lookup(json, (id, value) => {
                    self.#data.put(id, value);
                }, getArg(json, "id"), getArgs(json, "value"));
            },

            QUERY(json){
                if (!self.#initialized) return;
                if (self.#amIResponsibleOf(getArg(json, "id"))){
                    
                }
            },

            LOOKUP(json){
                if (!self.#initialized) return;
                self.#generic_lookup (json, (ip, port, id) => {
                    let val = data.get(id);
                    send_json({
                        request: 'RESPONSE',
                        id: json.id,
                        val: val || "",
                        present: !!val
                    }, ip, port);
                }, getArgs(json, "ip", "port", "id"));
            },

            JOIN(json){
                if (!self.#initialized) return;
                self.#add_node(getArg(json, "ip"), getArg(json, "port"));
            },

            CHECK(json){
                if (!self.#initialized) return;
                self.#check(json);
            },

            NAV(json){
                //ici, on ne sait pas quel était le noeud auquel on avait envoyé la demande à la base. Pas grave,
                //on va utiliser le fait qu'au final peu importe à qui on redemande : on renvoie la nouvelle demande
                //au noeud qui nous a envoyé le NAV
                self.join_network(self.#entry.ip, self.#entry.port);
            },

            OK(json){
                //on va fire un event
                self.#initialized = true;
                self.#onJoin(getArgsNamed(json, "ip_pred", "port_pred", "id_pred", "ip_succ", "port_succ", "id_succ", "id", "data"));
            },

            UPDATE(json){
                if (!self.#initialized) return;
                self.#neighbors.next = new RemoteNode(
                    getArg(json, "id"),
                    getArg(json, "ip"),
                    getArg(json, "port"),
                )
            },

            RESPONSE(json){
                if (!self.#initialized) return;
                if (json.present){
                    node.onResponseRecevied(json.value, json.id);
                } else {
                    node.onDataNotFound(json.id);
                }
            }
        }

    }

    #interpretRequest(json){
        let req = json.request;
        if (!req) throw "No request in message";
        let handler = this.#handlers[req];
        if (!handler) throw "Unrecognized request";
        return handler(json);
    }

    /**
     * Sends a join request to the specified node
     * @param {string} gate_ip
     * @param {number} gate_port
     */
    join_network(gate_ip, gate_port){
        this.#entry = {ip : gate_ip, port: gate_port};
        send_json({
            request: 'JOIN',
            ip: this.ip,
            port: this.port
        }, gate_ip, gate_port);
    }

    quit(){
        //QUIT
    }

    /**
     * Sets this server as the first node in a new chord network
     */
    start_inital_server(){
        this.id = Math.floor(Math.random() * maxID);
        this.#neighbors = {
            previous: new RemoteNode(this.id, this.ip, this.port),
            next : new RemoteNode(this.id, this.ip, this.port)
        };
        this.#initialized = true;
        this.emit('init', this.id);
    }

    print_info(){
        console.log("NODE INFO :", node_string(this));
        console.log("Next neighbor :", node_string(this.#neighbors.next));
        console.log("Previous neighbor :", node_string(this.#neighbors.previous));
    }

    print_link(){
        console.log(this.#neighbors.previous.id, "->", this.id, "->", this.#neighbors.next.id);
    }

    print_value(){
        this.#data.print();
    }
};

export function join_network(my_ip, my_port, gate_ip, gate_port){
    let server = new ChordServer(my_ip, my_port);
    server.join_network(gate_ip, gate_port);


    return server;
}